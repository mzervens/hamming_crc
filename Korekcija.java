import java.util.Arrays;

/**
 * Autors: Matīss Zērvēns
 * Studentu apliecības #: mz12038
 * Created by Matiss on 2017.10.26..
 */

/*
Izmantotie materiāli

Kursa grāmatas fragments
wikipēdija https://en.wikipedia.org/wiki/Cyclic_redundancy_check
 */
class CRC {
    private String gx = ""; // dalīšanas polinoms

    private int degree = 0; // dalīšanas polinoma augstākā pakāpe

    public CRC(String pGx) {
        degree = pGx.length() - 1; // degree sanāk šādi, ja ievadīs korekti polinomu, kur kreisais bits būs vienmēr 1
        gx = pGx;
    }

    /**
     * Uzģenerē pārbaudes bitu virkni
     *
     * @param data binary string eg "11001"
     * @return binary string
     */
    public String generateRedundency(String data)
    {
        return generateRedundency(data, false);
    }

    /**
     * Metode vai nu ģenerēs pārbaudes bitu virkni vai arī pārbaudīs kļūdu datos
     *
     * @param data binary string eg "11001"
     * @param check binary string eg "11001"
     * @return binary string
     */
    public String generateRedundency(String data, boolean check)
    {
        char[] redundency = new char[degree];

        if(check == false) {
            Arrays.fill(redundency, '0');
        }
        else
        {
            redundency = "".toCharArray();
        }

        char[] dataChars = (data + String.valueOf(redundency)).toCharArray();
        char[] gxChars = gx.toCharArray();

        for(int i = 0; i < dataChars.length; i++)
        {
            if(dataChars[i] == '0')
            {
                continue;
            }

            if((i + gxChars.length) > dataChars.length)
            {
                break;
            }


            for(int n = 0; n < gxChars.length; n++)
            {
                char res = Utils.XOR(gxChars[n], dataChars[i + n]);
                dataChars[i + n] = res;
            }

        }

        if(!check) {
            redundency = Arrays.copyOfRange(dataChars, data.length(), dataChars.length);
        }
        else
        {
            redundency = Arrays.copyOfRange(dataChars, data.length() - degree, dataChars.length);
        }

        return String.valueOf(redundency);
    }

    /**
     * Izveido koda vārdu
     *
     * @param data binary string eg "11001"
     * @return binary string ready to be sent over wires
     */
    public String encode(String data)
    {
        String redundency = generateRedundency(data);
        return  data + redundency;
    }

    /**
     * Pārbauda kļūdu datos
     *
     * @param code binary string with cosisting of data + check bits
     * @return
     */
    public String check(String code)
    {
        String remainded = generateRedundency(code, true);
        return remainded;
    }

}


/*
Izmantotie materiāli:

wikipēdijas raksts https://en.wikipedia.org/wiki/Hamming_code
Mani bakalaura mājasdarbi kursā Lietiškie algoritmi
Kursa grāmatas fragments

 */
class HammingCode {
    private int m = 2; // paritātes bitu skaits
    private int n; // koda virkņu garums bitos
    private int k; // datu bitu skaits

    public HammingCode(int dataLength) {
        k = (int) Math.pow(2, m) - m - 1; // k = 2^m - m - 1

        while (true) {

            k = (int) Math.pow(2, m) - m - 1;

            if (k >= dataLength) {
                break;
            }

            m++;
        }

        n = ((int) Math.pow(2, m)) - 1;
    }

    /**
     * Pārvērš bitu datu virkni par Hemminga koda virkni
     *
     * Vispirms uzģenerē koda virkni, tad aizpilda no kreisās uz labo visus datu bitus
     * Pēc tam izrēķina paritātes bitu vērtības arī ejot no kreisās uz labo
     *
     * @param data binary string eg "1010"
     * @return binary string
     */
    public String encode(String data)
    {
        char[] code = new char[n];
        Arrays.fill(code, '0');

        char[] dataChars = data.toCharArray();

        int codeIndx = 0;

        for(int i = 0; i < data.length(); i++)
        {
            codeIndx = setToNextDataIndx(codeIndx);
            code[codeIndx] = dataChars[i];
            codeIndx++;
        }

        int parityIndx = 1;

        for(int i = 0; i < m; i++) {
            int bitSum = 0;

            for (int j = 0; j < code.length; j++) {
                int cIndx = j + 1;
                if (cIndx == parityIndx) {
                    continue;
                }

                if ((parityIndx & cIndx) != 0) { //In general each parity bit covers all bits where the bitwise AND of the parity position and the bit position is non-zero.
                    if (code[j] == '1') {
                        bitSum += 1;
                    }
                }
            }

            bitSum = bitSum % 2;

            if (bitSum == 1)
            {
                code[parityIndx - 1] = '1';
            }
            else
            {
                code[parityIndx - 1] = '0';
            }

            parityIndx++;
            parityIndx = setToNextParityIndx(parityIndx);
        }

        return String.valueOf(code);
    }

    /**
     * Pārbauda Hemminga kodā kļūdas pārbaudot paritāšu bitu sagaidāmās un reālās vērtības
     *
     * Ja visi paritātes biti sakrita, tad pieņem, ka nav kļūda
     * Ja vienā no paritātes bitiem bija nesakritība, tad attiecīgais paritātes bits ir nekorekts
     * Ja vairāki paritātes biti nesakrīt, tad to indeksu summa ir kļūdainais bits
     *
     * @param code binary string eg "1100111"
     * @return binary string
     */
    public String check(String code)
    {
        int parityIndx = 1;
        char[] codeChars = code.toCharArray();

        boolean mistake = false;
        int mistakeIndx = 0;

        for(int i = 0; i < m; i++) {
            int bitSum = 0;
            char bitSumChar = '0';

            for (int j = 0; j < codeChars.length; j++) {
                int cIndx = j + 1;
                if (cIndx == parityIndx) {
                    continue;
                }

                if ((parityIndx & cIndx) != 0) { //In general each parity bit covers all bits where the bitwise AND of the parity position and the bit position is non-zero.
                    if (codeChars[j] == '1') {
                        bitSum += 1;
                    }
                }
            }

            bitSum = bitSum % 2;

            if (bitSum == 1)
            {
                bitSumChar = '1';
            }
            else
            {
                bitSumChar = '0';
            }

            if(codeChars[parityIndx - 1] != bitSumChar) // mistake
            {
                mistake = true;
                mistakeIndx += parityIndx;
            }

            parityIndx++;
            parityIndx = setToNextParityIndx(parityIndx);
        }

        if(!mistake) // no mistake detected or more than one bit error
        {
            return code;
        }
        else // detected one bit error
        {
            if(codeChars[mistakeIndx - 1] == '1')
            {
                codeChars[mistakeIndx - 1] = '0';
            }
            else
            {
                codeChars[mistakeIndx - 1] = '1';
            }

            return String.valueOf(codeChars);
        }
    }

    private int setToNextDataIndx(int indx)
    {
        while(Utils.isPowerOfTwo(indx + 1))
        {
            indx++;
        }

        return indx;
    }

    private int setToNextParityIndx(int indx)
    {
        while(!Utils.isPowerOfTwo(indx))
        {
            indx++;
        }

        return  indx;
    }

    public void debug()
    {
        System.out.printf("%d,%d,%d", m, n, k);
    }

}

// dažas palīgmetodes dzīves uzlabošanai
class Utils
{
    static boolean isPowerOfTwo(int x)
    {
        return (x & (x - 1)) == 0;
    }

    static String changeCharAt(char c, int indx, String str)
    {
        char[] data = str.toCharArray();
        data[indx] = c;
        return String.valueOf(data);
    }

    static void runHammingCheckTest(String code, HammingCode c)
    {
        System.out.println("Pārbaudam kodu: " + code);
        System.out.println("Koriģētais kods: " + c.check(code));

        if(c.check(code).equals(code))
        {
            System.out.println("Netika atrasta kļūda");
        }

        System.out.println();

    }

    static char XOR(char a, char b)
    {
        if(a == b)
        {
            return '0';
        }
        else
        {
            return '1';
        }
    }
}


/**
 * Veic testus ar noprogrammētajām klasēm
 */
public class Korekcija
{

    private static void runHamming()
    {
        System.out.println("HAMMING");

        // inicializē kodu ģenerēšanas klasi, kas būs paredzēta, lai kodētu 4 bitu datu virknes
        HammingCode code = new HammingCode(4);

        // pārkodē 3 datu virknes "0110", "1010", "0101" uz Hemminga koda virknēm, kuras varētu sūtīt pa tīklu
        String encoded1 = code.encode("0110"); // "0110" iekodējas par 1100110
        String encoded2 = code.encode("1010"); // "1010" iekodējas par 1011010
        String encoded3 = code.encode("0101"); // "0101" iekodējas par 0100101
        String encoded4 = code.encode("1110"); // "1110" iekodējas par 0010110
        String encoded5 = code.encode("0011"); // "0011" iekodējas par 1000011

        encoded1 = Utils.changeCharAt('1', 2, encoded1); // sabojājam trešo bitu no "1100110" iegūstam 1110110
        encoded2 = Utils.changeCharAt('0', 0, encoded2); // sabojājam pirmo bitu no "1011010" iegūstam 0011010
        encoded3 = Utils.changeCharAt('1', 5, encoded3); // sabojājam sesto bitu no "0100101" iegūstam 0100111

        encoded4 = Utils.changeCharAt('1', 0, encoded4);
        encoded4 = Utils.changeCharAt('1', 6, encoded4); // sabojājam pirmo un pēdējo no "0010110" iegūstam 1010111

        Utils.runHammingCheckTest(encoded1, code);
        Utils.runHammingCheckTest(encoded2, code);
        Utils.runHammingCheckTest(encoded3, code);
        // šos 3 vajadzētu salabot

        Utils.runHammingCheckTest(encoded4, code);
        // vairāk kā 1 kļūda, nespēs salabot

        Utils.runHammingCheckTest(encoded5, code);
        // vajadzētu neatrast kļūdu
    }

    private static void runCRC()
    {
        System.out.println();
        System.out.println("CRC");

        // inicializē kodu ģenerēšanas klasi, kas strādās ar dalīšanas polinomu "1011"
        CRC code = new CRC("1011");
        String encoded = code.encode("11010011101100"); // iekodē uz 11010011101100100

        System.out.println(code.check(encoded)); // atlikumā iegūst 000, tātad nebija kļūda

        encoded = Utils.changeCharAt('0', 1, encoded); // sabojā otro bitu, iegūst 10010011101100100
        System.out.println(code.check(encoded)); // atlikumā iegūst 010, tātad bija kļūda

        encoded = Utils.changeCharAt('1', 2, encoded); // pārmaina otro bitu, iegūst 10110011101100100
        System.out.println(code.check(encoded)); // atlikumā iegūst 011, tātad bija kļūda

        encoded = Utils.changeCharAt('0', 14, encoded); // pārmaina otro bitu, iegūst 10110011101100000
        System.out.println(code.check(encoded)); // atlikumā iegūst 111, tātad bija kļūda

    }

    public static void main(String args[])
    {
        runHamming();
        runCRC();

    }

}
